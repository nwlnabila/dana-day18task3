package com.day18task3.controller;

import com.day18task3.model.StudentSubject;
import com.day18task3.repository.StudentRepository;
import com.day18task3.repository.StudentSubjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/studentsubject")
public class StudentSubjectController {
    // Spring Boot Logger
    public static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    StudentSubjectRepository studentSubjectRepository;

    // -----------------Register Subject----------------------
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<StudentSubject> insertStudentSubject(@RequestBody StudentSubject studentsubject) {
        log.info("Adding Subject to Student Id: {}", studentsubject.getId());
        int result = studentSubjectRepository.insert(studentsubject);

        if (result > 0) {
            log.info("Adding Student Id {} success!", studentsubject.getId());
            return new ResponseEntity<StudentSubject>(studentsubject, HttpStatus.CREATED);
        } else {
            log.info("Adding Student Id {} failed!", studentsubject.getId());
            return new ResponseEntity<StudentSubject>(studentsubject, HttpStatus.CONFLICT);
        }
    }

    // ------------------Get All Student Subject-----------------------
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<StudentSubject>> getAllStudentSubject() {
        log.info("Finding All Subject Taken by Students...");
        List<StudentSubject> studentsubjects = studentSubjectRepository.findAll();
        log.info("[FIND_ALL] {}", studentsubjects);
        return new ResponseEntity<List<StudentSubject>>(studentsubjects, HttpStatus.OK);
    }

    // -----------------Get Student Subject by Student ID-----------------------
    @RequestMapping(value = "/student/{studentid}", method = RequestMethod.GET)
    public ResponseEntity<List<StudentSubject>> getStudentSubjectByStudentId(@PathVariable("studentid") int studentid) {
        log.info("[FIND_BY_STUDENTID] :{}", studentid);
        List<StudentSubject> studentsubjects = studentSubjectRepository.findByStudentId(studentid);
        log.info("{}", studentsubjects);
        return new ResponseEntity<List<StudentSubject>>(studentsubjects, HttpStatus.OK);
    }

    // -----------------Get Student Subject by Subject ID-----------------------
    @RequestMapping(value = "/subject/{subjectid}", method = RequestMethod.GET)
    public ResponseEntity<List<StudentSubject>> getStudentSubjectBySubjectId(@PathVariable("subjectid") int subjectid) {
        log.info("[FIND_BY_SUBJECTID] :{}", subjectid);
        List<StudentSubject> studentsubjects = studentSubjectRepository.findBySubjectId(subjectid);
        log.info("{}", studentsubjects);
        return new ResponseEntity<List<StudentSubject>>(studentsubjects, HttpStatus.OK);
    }

    // -----------------Update Attendance---------------------
    @RequestMapping(value = "/update/attendance/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> updateAttendance(@PathVariable("id") int id) {
        log.info("Updating Attendance, Id :{}", id);
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        String todayDate = now.format(dtf);

        StudentSubject studentSubject = studentSubjectRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        log.info("{}", studentSubject);
        String attendance = studentSubject.getAttendance();

        if (attendance.equals(todayDate)) {
            return new ResponseEntity<String>("already attend today", HttpStatus.CONFLICT);
        } else {
            studentSubject.setAttendance(todayDate);
            studentSubjectRepository.updateAttendance(id, todayDate);
            return new ResponseEntity<String>("success", HttpStatus.OK);
        }
    }

    // -----------------Get Score by ID-----------------------
    @RequestMapping(value = "/score/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getScoreByStudentIdAndId(@PathVariable("id") int id) {
        log.info("GET SCORE, id : {}", id);
        StudentSubject studentsubject = studentSubjectRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        log.info("{}", studentsubject);

        if (!studentsubject.getAttendance().equals("0000-00-00")) {
            if (studentsubject.getScore() != 0) {
                return new ResponseEntity<Integer>(studentsubject.getScore(), HttpStatus.OK);
            } else {
                return new ResponseEntity<String>("your score not ready yet", HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<String>("your not attend the exam yet", HttpStatus.OK);
        }
    }

    // -------------------Delete Student Subject------------------------
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteStudentSubjectById(@PathVariable("id") int id) {
        log.info("[DELETE] :{}", id);
        int result = studentSubjectRepository.deleteById(id);
        log.info("rows affected: {}", result);
        return new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
